﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoomChecker.Models
{
    public class Room
    {
        public string name { get; set; }
        public string email { get; set; }
        public int priority { get; set; }
        public int capacity { get; set; }
        public bool hasProjector { get; set; }
        public bool hasVC { get; set; }

        public Room(string name, string email, int priority = 10, int capacity = 10, bool hasProjector = false, bool hasVC = false)
        {
            this.name = name;
            this.email = email;
            this.priority = priority;
            this.capacity = capacity;
            this.hasProjector = hasProjector;
            this.hasVC = hasVC;
        }
    }
}