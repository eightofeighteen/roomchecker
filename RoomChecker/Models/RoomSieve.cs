﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Exchange.WebServices.Data;

namespace RoomChecker.Models
{
    using ListApp = List<Appointment>;
    using DictApp = Dictionary<Room, List<Appointment>>;
    public class RoomSieve
    {
        List<SingleRoomSieve> sieve = new List<SingleRoomSieve>();
        public RoomSieve(DictApp appts)
        {
            foreach (Room room in appts.Keys)
            {
                SingleRoomSieve s = new SingleRoomSieve(room, appts[room]);
                sieve.Add(s);
            }
        }

        public Dictionary<Room, int> getSpaces(DateTime start, int duration)
        {
            Dictionary<Room, int> spaces = new Dictionary<Room, int>();

            foreach (SingleRoomSieve s in sieve)
            {
                spaces.Add(s.room, s.CheckAppointment(start, duration));
            }

            return spaces;
        }
    }
}