﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Exchange.WebServices.Data;

namespace RoomChecker.Models
{
    using Sieve = Dictionary<DateTime, bool>;
    using ListApp = List<Appointment>;
    using DictApp = Dictionary<Room, List<Appointment>>;
    public class SingleRoomSieve
    {
        public Room room { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        private Sieve sieve = new Sieve();
        public static int SUCCESS = 0;
        public static int ADJACENT = 1;
        public static int OVERLAP = 2;


        public void AddAppointments(ListApp appts)
        {
            foreach (Appointment a in appts)
            {
                DateTime start = a.Start;
                int duration = (int)(a.End - a.Start).TotalMinutes;
                //Console.WriteLine(a.Subject + "\t" + duration);
                for (int i = 0; i < duration; i++)
                    sieve[start.AddMinutes(i)] = true;
            }
        }

        public int CheckAppointment(DateTime start, int duration)
        {
            bool okay = true;
            for (int i = 0; i < duration; i++)
            {
                if (sieve.Keys.Contains(start.AddMinutes(i)))
                    okay = false;
            }
            if (okay && (sieve.Keys.Contains(start.AddMinutes(-1)) || sieve.Keys.Contains(start.AddMinutes(duration)))) return ADJACENT;
            if (okay) return SUCCESS;
            return OVERLAP;
        }

        public SingleRoomSieve(Room room)
        {
            this.room = room;
        }

        public SingleRoomSieve(Room room, ListApp appts)
        {
            this.room = room;
            AddAppointments(appts);
        }
    }
}