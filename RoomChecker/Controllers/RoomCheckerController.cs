﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Exchange.WebServices.Data;

namespace RoomChecker.Controllers
{
    using ListApp = List<Appointment>;
    using DictApp = Dictionary<Models.Room, List<Appointment>>;
    using Room = Models.Room;
    using RoomSieve = Models.RoomSieve;
    using SingleRoomSieve = Models.SingleRoomSieve;
    public class RoomCheckerController : Controller
    {
        //
        // GET: /RoomChecker/

        RoomInformationDataContext context = new RoomInformationDataContext();
        static string username = "roommonitor";
        static string password = "Illovo555";

        static ListApp getAppointments(ExchangeService service, SearchFilter filter, string mailbox)
        {
            ListApp appointments = new List<Appointment>();
            // Limit the result set to 10 items.
            ItemView view = new ItemView(10);

            view.PropertySet = new PropertySet(ItemSchema.Subject,
                                               ItemSchema.DateTimeReceived,
                                               EmailMessageSchema.IsRead);

            // Item searches do not support deep traversal.
            view.Traversal = ItemTraversal.Shallow;

            // Define the sort order.
            view.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Descending);

            try
            {
                // Call FindItems to find matching calendar items. 
                // The FindItems parameters must denote the mailbox owner,
                // mailbox, and Calendar folder.
                // This method call results in a FindItem call to EWS.
                FindItemsResults<Item> results = service.FindItems(
                new FolderId(WellKnownFolderName.Calendar,
                    mailbox),
                    filter,
                    view);

                var propertySet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End, AppointmentSchema.Location);

                if (results.Items != null && results.Items.Count > 0)
                    service.LoadPropertiesForItems(results.Items, propertySet);

                foreach (Item item in results.Items)
                {
                    appointments.Add((Appointment)item);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while enumerating results: {0}", ex.Message);
            }
            return appointments;
        }

        static ListApp getEntriesForRoom(ExchangeService service, Room room)
        {
            // Validate the server certificate.
            // For a certificate validation code example, see the Validating X509 Certificates topic in the Core Tasks section.
            ListApp appts = new ListApp();
            try
            {
                appts = getAppointments(service, null, room.email);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return appts;
        }

        static DictApp getAppointmentsForRooms(List<Room> rooms)
        {
            DictApp appts = new DictApp();

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            service.Credentials = new WebCredentials(username, password);
            service.AutodiscoverUrl("spurnell@illovo.co.za");

            foreach (Room room in rooms)
            {
                appts.Add(room, getEntriesForRoom(service, room));
            }

            return appts;
        }

        static List<Room> FindRoom(DateTime start, int duration, int capacity, bool needsProjector, bool needsVC)
        {
            List<Room> rooms = new List<Room>();
            RoomSieve sieve = new RoomSieve(getAppointmentsForRooms(RoomList()));
            var spaces = sieve.getSpaces(start, duration);

            foreach (Room room in spaces.Keys)
            {
                if ((spaces[room] == SingleRoomSieve.SUCCESS || spaces[room] == SingleRoomSieve.ADJACENT) && room.capacity >= capacity)
                {
                    if (!needsProjector && !needsVC)
                    {
                        rooms.Add(room);
                    }
                    else if (needsProjector && needsVC)
                    {
                        if (room.hasProjector && room.hasVC)
                            rooms.Add(room);
                    }
                    else if (needsProjector)
                    {
                        if (room.hasProjector)
                            rooms.Add(room);
                    }
                    else if (needsVC)
                    {
                        if (room.hasVC)
                            rooms.Add(room);
                    }
                }
            }

            /**
             * Sorts so that you always get the smallest room available.
             * Then sorts so that you don't get expensive VC equipment and projectors if you don't need it.
             * */
            if (needsProjector && needsVC)
                rooms = rooms.OrderBy(o => o.capacity).ToList();
            else if (!needsProjector && !needsVC)
                rooms = rooms.OrderBy(o => o.capacity).ThenBy(o => o.hasVC).ThenBy(o => o.hasProjector).ToList();
            else if (needsProjector)
                rooms = rooms.OrderBy(o => o.capacity).ThenBy(o => o.hasVC).ToList();
            else if (needsVC)
                rooms = rooms.OrderBy(o => o.capacity).ThenBy(o => o.hasProjector).ToList();

            return rooms;
        }

        static List<Room> RoomList()
        {
            List<Room> rooms = new List<Room>();

            RoomInformationDataContext context = new RoomInformationDataContext();
            var q =
                from a in context.DBRooms
                select a;
            foreach (var r in q)
            {
                rooms.Add(new Room(r.name, r.email, r.priority, r.capacity, r.hasProjector, r.hasVC));
            }

            return rooms;
        }

        public ActionResult SelectRoom()
        {
            return View();
        }

        public ActionResult Search(DateTime start_date, int start_hour, int start_minute, int duration, int capacity, bool projector, bool vc)
        {
            DateTime start = start_date;
            start = start.AddHours(start_hour).AddMinutes(start_minute); ;
            //start = start.AddMinutes(start_minute);
            ViewBag.start = start;
            ViewBag.duration = duration;
            ViewBag.capacity = capacity;
            ViewBag.projector = projector;
            ViewBag.vc = vc;
            return View(FindRoom(start, duration, capacity, projector, vc));
        }

        public ActionResult Index()
        {
            return View();
        }
	}
}